from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

title = 'Transcript assembly and quantification by RNA-Seq reveals unannotated transcripts and isoform switching during cell differentiation'

# initialise stop words
english_stops = set(stopwords.words('english'))

#tokenize title
title_tokens = word_tokenize(title)

#remove stop words
words = [word for word in title_tokens if word not in english_stops]

# init bag_of_words
def bag_of_words(words):
	return dict([word, True] for word in words)

#create bag_of_words
bag = bag_of_words(words)


print bag

