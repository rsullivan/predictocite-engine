from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from nltk.corpus import stopwords

english_stops = set(stopwords.words('english'))
train_set = ('Transcript assembly and quantification by RNA-Seq reveals unannotated transcripts and isoform switching during cell differentiation', 'Function annotation of the rice transcriptome at single-nucleotide resolution by RNA-seq')
test_set = ('A transcriptome atlas of rice cell types uncovers cellular, functional and developmental hierarchies', 'Differential gene and transcript expression analysis of RNA-seq experiments with TopHat and Cufflinks')

vectorizer = CountVectorizer(stop_words=english_stops)
vectorizer.fit_transform(train_set)#learn the vocabulary dictionary and return term-document matrix

print vectorizer.vocabulary_#will be unordered because is dict/bag_of_words

smatrix = vectorizer.transform(test_set)
'''	
	transform documents to document-term matrix. uses vocab from fit_transform
	
'''
print 'Mtrain (term-frequency matrix)', smatrix.todense()
'''
todense() turns the matrix into a dense format which looks more like a term-frequency document vector. 
It's based on the vocab created from fit_transform(train_set)
'''

'''
now to normalise the vector using L2-norm

'''

term_frequency_matrix = smatrix.todense()

tfidf = TfidfTransformer(norm='l2')
tfidf.fit(term_frequency_matrix) #idf calculated for frequency_term_matrix 

print 'IDF: ', tfidf.idf_

# now transform to tf-idf weight matrix

tf_idf_matrix = tfidf.transform(term_frequency_matrix)

print  'tf-idf weights', tf_idf_matrix.todense()


