try:
	from setuptools import setup
except ImportError:
	from distutils.core import setup

config = {
	'description': 'predictocite-engine does all the heavy lifting',
	'author': 'Robert Sullivan',
	'url': 'http://some-url',
	'author_email': 'robertjsullivan.esq@gmail.com',
	'version': '0.1',
	'install_requires': [
	'nose==1.3.4',
	'pymongo==2.7.2',
	'numpy==1.9.1'],
	'packages': ['predictocite'],
	'scripts': [],
	'name': 'predictocite'
}

setup(**config)